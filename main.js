
document.getElementById("btn_next_1").addEventListener("click",function(){
    document.getElementById("step_2").style.display="block";
});
document.getElementById("btn_back_2").addEventListener("click",function(){
    document.getElementById("step_2").style.display="none";
});
document.getElementById("btn_next_2").addEventListener("click",function(){
    document.getElementById("step_3").style.display="block";
});
document.getElementById("btn_back_3").addEventListener("click",function(){
    document.getElementById("step_3").style.display="none";
});

function show(a,b) {
    document.getElementById('taginput').style.display="none";
    document.getElementById('exp_taginput').style.display="none";
    document.getElementById('eduform').style.display="none";
    document.getElementById('display_edu_info').style.display="none";
    document.getElementById('expform').style.display="none";
    document.getElementById('display_exp_info').style.display="none";
    document.getElementById(a).style.display="inline-block";
    document.getElementById(b).style.display="inline-block";
    
}

document.getElementById("edu_form_btn").addEventListener("click",
function() {
    let edu_info={
        Name:"xyz",
        from:null,
        to:null,
        stream:"abc"
    };
    edu_info.Name=document.getElementById("instituion_name").value;
    edu_info.from=document.getElementById("from_date").value;
    edu_info.to=document.getElementById("to_date").value;
    edu_info.stream=document.getElementById("stream").value;
    let p=document.createElement("p");
    let s=document.createElement("span");
    let c=document.createElement("i");
    p.setAttribute("class","p_edu_info");
    c.setAttribute("class","fa fa-times-circle");
    s.setAttribute("class","rmv_btn");
    s.setAttribute("onclick","rmv(this)")
    s.appendChild(c);
    let txt=document.createTextNode(edu_info.Name+" ("+ edu_info.stream+" ) "+edu_info.from+" - "+edu_info.to);
    p.appendChild(s);
    p.appendChild(txt);
   document.getElementById("display_edu_info").appendChild(p); 
});
document.getElementById("exp_form_btn").addEventListener("click",
function() {
    let exp_info={
        Name:"xyz",
        from:null,
        to:null,
        post:"abc"
    };
    exp_info.Name=document.getElementById("exp_org_name").value;
    exp_info.from=document.getElementById("exp_from_date").value;
    exp_info.to=document.getElementById("exp_to_date").value;
    exp_info.post=document.getElementById("exp_post").value;
    let p=document.createElement("p");
    let s=document.createElement("span");
    let c=document.createElement("i");
    p.setAttribute("class","p_edu_info");
    c.setAttribute("class","fa fa-times-circle");
    s.setAttribute("class","rmv_btn");
    s.setAttribute("onclick","rmv(this)")
    s.appendChild(c);
    let txt=document.createTextNode(exp_info.Name+" ("+ exp_info.post+" ) "+exp_info.from+" - "+exp_info.to);
    p.appendChild(s);
    p.appendChild(txt);
   document.getElementById("display_exp_info").appendChild(p); 
});

function rmv(t){    
    t.parentNode.remove();
}